import { Component, Input, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'ava-inline-text-edit',
  template: `
    
    <input
      (blur)="confirm($event)"
      (keydown.enter)="confirm($event)"
      (keydown.escape)="isEditing = false"
      type="text" 
      #input
      [ngModel]="value"
      *ngIf="isEditing">
    
    <span
      (click)="clickOnText()"
      *ngIf="!isEditing"
    >{{value}}</span>
  `,
  styles: [
  ]
})
export class InlineTextEditComponent {
  @ViewChild('input') input: ElementRef<HTMLInputElement>
  @Input() value: string;
  @Output() edit = new EventEmitter<string>();
  isEditing = false;

  confirm(event: KeyboardEvent | FocusEvent): void {
    const text = (event.target as HTMLInputElement).value;
    // this.value = text;
    this.edit.emit(text);
    this.isEditing = false;

  }

  clickOnText(): void {
    this.isEditing = true;
    setTimeout(() => {
      this.input.nativeElement.focus();
    });
  }
}
