import { Directive, ElementRef, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[avaUrl]'
})
export class UrlDirective {
  @Input() avaUrl: string;
  @HostBinding('style.text-decoration') deco = 'underline';
  @HostBinding('style.color') color = 'blue';
  @HostBinding('style.cursor') cursor = 'pointer';

  @HostListener('click')
  clickMe(): void {
    window.open(this.avaUrl);
  }

  constructor(el: ElementRef) {
    el.nativeElement.style.fontSize = '50px'
  }
}
