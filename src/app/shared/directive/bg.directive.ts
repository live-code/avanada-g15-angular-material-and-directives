import { Directive, HostBinding, Input } from '@angular/core';

// attribute directive
@Directive({
  selector: '[avaBg]'
})
export class BgDirective {
  @Input() avaBg: string;
  @Input() color: string;

  @HostBinding('style.background') get getBg(): string {
    return this.avaBg;
  }

  @HostBinding('style.color') get getColor(): string {
    return this.color;
  }


}
