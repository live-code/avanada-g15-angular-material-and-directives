import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { MatDialog } from '@angular/material/dialog';
import { ModalEditComponent } from './components/modal-edit.component';
import { UserTable } from './components/users-table.component';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'ava-dashboard',
  template: `

    <ava-users-table 
      [users]="users"
      (deleteUser)="deleteHandler($event)"
      (openEditModal)="modalEditUserHandler($event)"
      (inlineEdit)="inlineEditFieldHandler($event)"
      (dragRow)="drop($event)"
    ></ava-users-table>

  `,

})
export class DashboardComponent implements OnInit {
  users: User[];

  constructor(
    private http: HttpClient,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => this.users = res )
  }

  deleteHandler(user: User): void {
    this.http.delete(`https://jsonplaceholder.typicode.com/users/${user.id}`)
      .subscribe(res => {
        this.users = this.users.filter(u => u.id !== user.id);
      });
  }

  modalEditUserHandler(user: Partial<User>): void {
    const dialogRef = this.dialog.open(ModalEditComponent, {
      data: user
    });

    dialogRef.afterClosed().subscribe((updatedUser: User) => {
      this.edit(updatedUser);
    });
  }


  // inlineEditFieldHandler(user: User, newText: string, key: string): void {
  // inlineEditFieldHandler(event: { user: User, newText: string, key: string }): void {
  // inlineEditFieldHandler({ user, newText, key }: { user: User, newText: string, key: string }): void {
  // inlineEditFieldHandler(event: { user: User, newText: string, key: string }): void {
  inlineEditFieldHandler(event: UserTable): void {
    const { user, newText, key } = event;
    const clonedUser = {...user};
    clonedUser[key] = newText;
    this.edit(clonedUser);
  }


  edit(user: User): void {
    // snapshot previous state
    const previousUsers = JSON.stringify(this.users)
    // optimistic update
    this.users = this.users.map(u => {
      return u.id === user.id ? { ...u, ...user } :  u;
    });

    this.http.patch<User>(`https://jsonplaceholder.typicode.com/users/${user.id}`, user)
      .subscribe(
        res => {
          // no need for optimistic update
        },
        err => {
          // rollback previous state
          this.users = JSON.parse(previousUsers)
        }
      );
  }

  drop(ids: { id: number, index: number }[]): void {
    console.log('HTTP patch - update ids', ids)
  }
}
