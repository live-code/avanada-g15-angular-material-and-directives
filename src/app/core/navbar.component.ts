import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'ava-navbar',
  template: `
    <mat-toolbar color="primary">
      <button
        routerLink="/"
        mat-icon-button class="example-icon" aria-label="Example icon-button with menu icon">
        <mat-icon>home</mat-icon>
      </button>
      <span>My App</span>
      <span class="example-spacer"></span>
      <button
        routerLink="dashboard"
        mat-icon-button class="example-icon favorite-icon" aria-label="Example icon-button with heart icon">
        <mat-icon>space_dashboard</mat-icon>
      </button>
      <button
        routerLink="login"
        matTooltip="Login"
        matTooltipPosition="below"
        matTooltipHideDelay="100000"
        matTooltipClass="my-button"
        mat-icon-button class="example-icon favorite-icon" aria-label="Example icon-button with heart icon">
        <mat-icon>lock</mat-icon>
      </button>
      
      <button
        routerLink="stats"
        mat-icon-button class="example-icon" aria-label="Example icon-button with share icon">
        <mat-icon>assessment</mat-icon>
      </button>
    </mat-toolbar>
  `,
  styles: [`
    .example-spacer {flex: 1 1 auto; }
   
  `]
})
export class NavbarComponent implements OnInit {

  constructor(private authService: AuthService) {
    setTimeout(() => {
      authService.auth.next(true)
    }, 3000)
    setTimeout(() => {
      authService.auth.next(false)
    }, 6000)

  }

  ngOnInit(): void {
  }

}
