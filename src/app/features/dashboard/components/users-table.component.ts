import { Component, Input, EventEmitter, OnInit, Output } from '@angular/core';
import { User } from '../../../model/user';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

export interface UserTable {
  user: User;
  newText: string;
  key: string;
}
@Component({
  selector: 'ava-users-table',
  template: `


    <table class="example-list" cdkDropList (cdkDropListDropped)="drop($event)">
      <tr>
        <th>Username</th>
        <th>email</th>
        <th>City</th>
        <th>action</th>
      </tr>
      <tr class="example-box" *ngFor="let user of users" cdkDrag>
        <td>
          <ava-inline-text-edit
            [value]="user.username"
            (edit)="inlineEditHandler(user, $event, 'username')"
          ></ava-inline-text-edit>

        </td>
        <td>
          <ava-inline-text-edit
            [value]="user.email"
            (edit)="inlineEditHandler(user, $event, 'email')"
          ></ava-inline-text-edit>
        </td>
        <td>
          <ava-inline-text-edit
            [value]="user.website"
            (edit)="inlineEditHandler(user, $event, 'website')"
          ></ava-inline-text-edit>
        </td>
        <td>
          <button mat-icon-button [matMenuTriggerFor]="menu">
            <mat-icon>more_vert</mat-icon>
          </button>

          <mat-menu #menu="matMenu">
            <button mat-menu-item (click)="deleteUser.emit(user)">
              <mat-icon>delete</mat-icon>
              <span>Delete</span>
            </button>
            <button mat-menu-item (click)="openEditModal.emit(user)">
              <mat-icon>edit</mat-icon>
              <span>Edit</span>
            </button>
          </mat-menu>

        </td>
      </tr>
    </table>
  `,
  styles: [`
    table {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
      background-color: white;
    }
/*
    tr:nth-child(even) {
      background-color: #dddddd;
    }*/
    .cdk-drag-preview {
      box-sizing: border-box;
      border-radius: 4px;
      box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),
      0 8px 10px 1px rgba(0, 0, 0, 0.14),
      0 3px 14px 2px rgba(0, 0, 0, 0.12);
    }

    
  `]
})
export class UsersTableComponent {
  @Input() users: User[];
  @Output() deleteUser = new EventEmitter<User>();
  @Output() openEditModal = new EventEmitter<User>();
  @Output() inlineEdit = new EventEmitter<UserTable>();
  @Output() dragRow = new EventEmitter<{ id: number, index: number }[]>();

  inlineEditHandler(user: User, newText: string, key: string): void {
    this.inlineEdit.emit({
      user,
      newText,
      key
    });
  }
  drop(event: CdkDragDrop<string[]>): void {
    moveItemInArray(this.users, event.previousIndex, event.currentIndex);
    const ids = this.users.map((user, index) => {
      return { id: user.id, index };
    });
    this.dragRow.emit(ids);
  }
}
