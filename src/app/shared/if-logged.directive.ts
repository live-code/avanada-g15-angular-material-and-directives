import { Directive, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../core/auth.service';

// structural directive
@Directive({
  selector: '[avaIfLogged]'
})
export class IfLoggedDirective {

  constructor(
    private template: TemplateRef<any>,
    private view: ViewContainerRef,
    private authService: AuthService
  ) {
    authService.auth
      .subscribe(isLogged => {
        if (isLogged) {
          view.createEmbeddedView(template);
        } else {
          view.clear();
        }
      });


  }

}
