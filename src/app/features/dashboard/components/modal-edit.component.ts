import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../../../model/user';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'ava-modal-edit',
  template: `
    <h2 mat-dialog-title>Install Angular</h2>
    <mat-dialog-content class="mat-typography">

      <form #f="ngForm" (ngSubmit)="saveHandler(f)">
<!--
        <input hidden type="text" [ngModel]="formData.id" name="id">
-->

        <mat-form-field class="example-form-field">
          <mat-label>Username</mat-label>
          <input matInput type="text" [ngModel]="formData.username" name="username">
        </mat-form-field>
        <mat-form-field class="example-form-field">
          <mat-label>Email</mat-label>
          <input matInput type="text" [ngModel]="formData.email" name="email">
        </mat-form-field>
        <button type="submit" hidden></button>
      </form>
      
    </mat-dialog-content>
    <mat-dialog-actions align="end">
      <button mat-button mat-dialog-close>Cancel</button>
      <button mat-button (click)="saveHandler(f)" cdkFocusInitial>Confirm</button>
    </mat-dialog-actions>

  `,
  styles: [
  ]
})
export class ModalEditComponent {
  formData: Partial<User> = { };

  constructor(
    private matDialog: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public user: Partial<User>
  ) {
    this.formData = user;
  }

  saveHandler(form: NgForm): void {
    const merged = {...this.formData, ...form.value};
    this.matDialog.close(merged)
  }
}
