import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-login',
  template: `
    <div [avaBg]="color" color="red">
      login works!
    </div>
    
    <button (click)="color = 'green'">green</button>
    <button (click)="color = 'cyan'">cyan</button>
    
    <hr>
    Visit the 
    <span avaUrl="http://www.google.com">web site</span>
    
    <hr>
    
    <button *avaIfLogged>If logged</button>
    
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {
  color = 'pink';

  constructor() { }

  ngOnInit(): void {
  }

}
