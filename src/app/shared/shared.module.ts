import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BgDirective } from './directive/bg.directive';
import { UrlDirective } from './directive/url.directive';
import { IfLoggedDirective } from './if-logged.directive';



@NgModule({
  declarations: [
  
    BgDirective,
       UrlDirective,
       IfLoggedDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    BgDirective,
    UrlDirective,
    IfLoggedDirective
  ]
})
export class SharedModule { }
